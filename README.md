# ics-bench

Collections of benchmark meshes and parameters


## Structure of the repository

Organizzation of the benchmarks has to be as follows

- `type of simulation` (folder)
    - `name of experiment` (folder)
        - `README.md` (Markdown file, with description, citations, and conditions for using the experiment)
        - `mesh file`
        - `simulation software` (folder)
            - `script` (runnable scripts with the particular simulation software)
            - `results` (folder) 
                - `file` (results achieved with script)
            - `README.md` (instructions on how to launch the experiments)

Example folder structure

- `contact` (type of simulation)
    - `drop-tower` (name of experiment)
        - `README.md` (description of experiment)
        - `drop-tower.e` (mesh file)
        - `mech` (simulation software)
            - `drop-tower.i` (script)
        - `utopia` (simulation software)
            - `drop-tower.json` (script)
            - `results` (folder)
                - `drop-tower-<experiment id>.<extension>` (result file)
            

## Docker containers

For facilitating the running of the various experiments please upload a Docker image with the exectuable to Docker Hub https://hub.docker.com (or create an automated build) and add the link to this file.

If specific version of a containers are required please specify in the file related to the simulation software

`type of simulation/name of experiment/simulation software/README.md` 

Example

`contact/drop-tower/utopia/README.md` 

### Utopia

For running experiments with utopia download the image with `docker pull utopiadev/utopia`. You can find the exectuables in `/utopia/bin`.




